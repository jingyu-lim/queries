select
city_name,
date_local,
service_type,
sum(number_of_rides) as number_of_rides,
sum(commission/exchange.exchange_one_usd) as commission,
sum(dax_fare/exchange.exchange_one_usd) as dax_fare_usd,
sum(pax_fare/exchange.exchange_one_usd) as pax_fare_usd,
sum(GMV_rides/exchange.exchange_one_usd) as gmv_rides_usd

from (
  select
  cities.city_name,
  cities.country_code,
  cities.country_id,
  bk.date_local,
  case
    when db.service_type = 0 and info.source = 'PARTNER' then 'API Instant'
    when db.service_type = 0 and info.source <> 'PARTNER' then 'APP Instant'
    when db.service_type = 1 then 'API Sameday 0.5'
  end as service_type,
  bk.receipt_commission as commission,
  bk.indicative_fare as dax_fare,
  cast(json_extract(info.quote, '$.amount') as double) as pax_fare,
  count_if(bk.state in ('ADVANCE_AWARDED','AWARDED','COMPLETED','DROPPING_OFF','PICKING_UP')) as number_of_rides,
  sum(case when bk.state IN ('ADVANCE_AWARDED','AWARDED','COMPLETED','DROPPING_OFF','PICKING_UP') then coalesce(bk.final_fare,0) + coalesce(receipt_tolls_and_others,0) + coalesce(receipt_booking_fee,0) else 0 end) as GMV_rides

  from public.prejoin_bookings as bk
  left join (select id, taxi_type_simple from public.taxi_types_v) as taxi on taxi.id = bk.taxi_type_id
  left join (select id, name as city_name, country_code, country_id from public.cities) as cities on cities.id = bk.city_id
  left join (
    select service_type, booking_code, delivery_code from grab_express.delivery_booking
    where concat(year,'-', month,'-', day) >= date_format(date_parse('2018-08-01', '%Y-%m-%d') -interval '1' day, '%Y-%m-%d')
    and concat(year,'-', month,'-', day) <= '2018-09-05'
  ) as db on db.booking_code = bk.booking_code
  left join (
    select delivery_code, source, quote from grab_express.delivery_info
    where concat(year,'-', month,'-', day) >= date_format(date_parse('2018-08-01', '%Y-%m-%d') -interval '1' day, '%Y-%m-%d')
    and concat(year,'-', month,'-', day) <= '2018-09-05'
  ) as info on info.delivery_code = db.delivery_code

  where taxi.taxi_type_simple = 'GrabExpress'
  and db.service_type in (0,1) /* only instant and SD 0.5 */
  and bk.user_id not in (999999999, 124923692) /* exclude GrabExpressTesting and Sameday 1.0 bookings */
  and bk.partition_date >= date_format(date_parse('2018-08-01', '%Y-%m-%d') -interval '1' day, '%Y-%m-%d')
  and bk.partition_date <= '2018-09-05'
  group by 1,2,3,4,5,6,7,8

  union all

  select
  cities.city_name,
  cities.country_code,
  cities.country_id,
  bk.date_local,
  'API Sameday 1.0' as service_type,
  bk.receipt_commission as commission,
  bk.indicative_fare as dax_fare,
  (orders.fare / power(10, orders.exponent)) as pax_fare,
  count_if(orders.order_state = 'COMPLETED') as number_of_rides,
  sum(case when orders.order_state = 'COMPLETED' then coalesce(bk.final_fare,0) + coalesce(bk.receipt_tolls_and_others,0) + coalesce(bk.receipt_booking_fee,0) else 0 end) as GMV_rides

  from grab_express.orders
  left join (select id, name as city_name, country_code, country_id from public.cities) as cities on cities.id = orders.city_id
  left join (
    select booking_code, date_local, city_id, receipt_commission, final_fare, receipt_tolls_and_others, receipt_booking_fee
    from public.prejoin_bookings
    where partition_date >= date_format(date_parse('2018-08-01', '%Y-%m-%d') -interval '1' day, '%Y-%m-%d')
    and partition_date <= '2018-09-05'
    and taxi_type_id in (9447,132)
  ) as bk on bk.booking_code = orders.playbook_id

  where orders.sender_id <> 999999999
  and concat(orders.year,'-', orders.month,'-', orders.day) >= date_format(date_parse('2018-08-01', '%Y-%m-%d') -interval '1' day, '%Y-%m-%d')
  and concat(orders.year,'-', orders.month,'-', orders.day) <= '2018-09-05'
  group by 1,2,3,4,5,6,7,8
) as ge_orders

left join (
  select country_id, exchange_one_usd, end_date from datamart.ref_exchange_rates
  where date_trunc ('month', date('2018-08-01')) = date_trunc ('month', end_date)
  and date_trunc ('year', date('2018-09-05')) = date_trunc ('year', end_date)
) as exchange on exchange.country_id = ge_orders.country_id
and date_trunc ('month', date( '2018-08-01')) = date_trunc ('month', end_date)
and date_trunc ('year', date('2018-09-05')) = date_trunc ('year', end_date)

group by 1,2,3;
