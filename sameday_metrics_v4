select
number_of_delivered_orders,
number_of_fulfilled_orders,
number_of_effective_orders,
number_of_orders,
number_of_orders_unique,
gmv_deliveries_usd,
total_number_of_retries,
total_time_to_allocate,
total_time_to_pickup,
total_time_to_dropoff,
number_met_allocation_sla,
number_met_pickup_sla,
number_met_dropoff_sla,
number_allocate_null,
number_pickup_null,
number_dropoff_null,
number_of_unbatched_orders,
number_of_originally_unallocated_orders,
number_of_dax_cancelled_orders,
number_with_candidates,
number_without_candidates,
platform,
sameday_version,
merchant_name,
country_code,
city,
hour_local,
pickup_long,
pickup_lat,
dropoff_long,
dropoff_lat,
created_at_local

from (
  select
  count_if(order_state IN ('COMPLETED')) as number_of_delivered_orders,  /* number of deliveries */
  count_if(order_state IN ('COMPLETED','RETURNED')) as number_of_fulfilled_orders,
  count_if(order_state IN ('COMPLETED','RETURNED', 'SCHEDULE_FAILED')) as number_of_effective_orders,
  count(1) as number_of_orders,                               /* number of orders/parcels */
	count_if(merchant_order_id is not null) as number_of_orders_unique,
	sum(case when order_state in ('COMPLETED','RETURNED', 'DROPPING_OFF','PICKING_UP')
	then delivery_fare/exchange_one_usd + coalesce(receipt_tolls_and_others/exchange_one_usd,0) + coalesce(receipt_booking_fee/exchange_one_usd,0) else 0 end) as gmv_deliveries_usd,
  sum(attempt) as total_number_of_retries,                    /* sum of retries for each order to get to final state */
  sum(time_to_allocate) as total_time_to_allocate,
  sum(time_to_pickup) as total_time_to_pickup,
  sum(time_to_dropoff) as total_time_to_dropoff,
  count_if(time_to_allocate <= 60) as number_met_allocation_sla,
  count_if(time_to_pickup <= 180) as number_met_pickup_sla,
  count_if(time_to_dropoff <= 360) as number_met_dropoff_sla,
	count_if(time_to_allocate is null) as number_allocate_null,
	count_if(time_to_pickup is null) as number_pickup_null,
	count_if(time_to_dropoff is null) as number_dropoff_null,
  sum(unbatched_order) as number_of_unbatched_orders,
  sum(originally_unallocated_order) as number_of_originally_unallocated_orders,
  sum(dax_cancelled_order) as number_of_dax_cancelled_orders,
	count_if(code is not null) as number_with_candidates,
	count_if(code is null) as number_without_candidates,
  case when order_source = 'PARTNER' then 'API' else order_source end as platform,
  sameday_version,
  merchant_name,
  country_code,
  city,
  hour(from_utc_timestamp(created_at, cities.time_zone)) as hour_local,
  date(from_utc_timestamp(created_at, cities.time_zone)) as created_at_local,
  pickup_long,
  pickup_lat,
  dropoff_long,
  dropoff_lat

  from (
    select
    order_id,
		playbook_id,
    created_at,
    city_id,
    order_state,
    order_source,
    attempt,
		fare / power(10, exponent) as delivery_fare,
    case when order_source = 'PARTNER' then sender_id else NULL end as merchant_id,  /* sender_id represents merchant id for API orders */
    case when taxi_type_id = 9447 then 'Sameday 1.0' else 'Sameday 0.5 (fallback)' end as sameday_version,
    date_diff('minute',created_at, picking_up_time) as time_to_allocate,
    date_diff('minute',created_at, dropping_off_time) as time_to_pickup,
    date_diff('minute',created_at, completion_time) as time_to_dropoff,
    pickup_long,
    pickup_lat,
    dropoff_long,
    dropoff_lat,

    /* unbatched */
    case when order_id not in (
      select distinct id from grab_datastore.express_order where state = 'PENDING_PICKUP' and vehicletypeid = 9447
      and concat(year,'-', month,'-', day) >= date_format(date_parse('2018-08-01', '%Y-%m-%d') -interval '1' day, '%Y-%m-%d')
      and concat(year,'-', month,'-', day) <= '2018-08-02'
    ) and taxi_type_id = 132 then 1 else 0 end as unbatched_order,

    /* unallcated (no driver accepted SD 1.0 job) */
    case when order_id not in (
      select distinct id from grab_datastore.express_order where state = 'PICKING_UP' and vehicletypeid = 9447
      and concat(year,'-', month,'-', day) >= date_format(date_parse('2018-08-01', '%Y-%m-%d') -interval '1' day, '%Y-%m-%d')
      and concat(year,'-', month,'-', day) <= '2018-08-02'
    ) and taxi_type_id = 132 then 1 else 0 end as originally_unallocated_order,

    /* dax cancelled order */
    case when order_id in (
      select distinct id
      from grab_datastore.express_order where vehicletypeid = 9447 and state in ('DRIVER_CANCELLED_DROPOFF','DRIVER_CANCELLED_PICKUP')
      and concat(year,'-', month,'-', day) >= date_format(date_parse('2018-08-01', '%Y-%m-%d') -interval '1' day, '%Y-%m-%d')
      and concat(year,'-', month,'-', day) <= '2018-08-02'
    ) and taxi_type_id = 132 then 1 else 0 end as dax_cancelled_order

    from grab_express.orders
    left join (
      select
      orderid,
      json_extract(pickuplocation,'$.Longitude') as pickup_long,
      json_extract(pickuplocation,'$.Latitude') as pickup_lat,
      json_extract(dropofflocation,'$.Longitude') as dropoff_long,
      json_extract(dropofflocation,'$.Latitude') as dropoff_lat,
      /* get timestamps from final attempt */
      try(map['PICKING_UP']) as picking_up_time,
      try(map['DROPPING_OFF']) as dropping_off_time,
      try(map['COMPLETED']) as completion_time

      from (
        select
        orderid,
        pickuplocation,
        dropofflocation,
        map_agg(cast(json_extract(orderevent, '$.State') as varchar),
        cast(from_unixtime( cast(json_extract(orderevent, '$.Timestamp') as bigint) ) as timestamp)) as map
        from grab_express.orders_metadata
        where concat(year,'-', month,'-', day) >= date_format(date_parse('2018-08-01', '%Y-%m-%d') -interval '1' day, '%Y-%m-%d')
        and concat(year,'-', month,'-', day) <= '2018-08-02'
        group by 1,2,3
      ) as status_time
    ) as time_metrics on time_metrics.orderid = orders.order_id

    where order_type = 'SAME_DAY'
    and sender_id <> 999999999    /* exclude GrabExpressTesting */
    and concat(year,'-', month,'-', day) >= date_format(date_parse('2018-08-01', '%Y-%m-%d') -interval '1' day, '%Y-%m-%d')
    and concat(year,'-', month,'-', day) <= '2018-08-02'

  ) as orders_data

  left join (select id, time_zone, name as city, country_code, country_id from public.cities) as cities on cities.id = orders_data.city_id
  left join (select merchant_name, merchant_grab_id from grab_express.merchants) as merchants on merchants.merchant_grab_id = orders_data.merchant_id
	/* get merchant order ID */
	left join (
    select order_id, merchant_order_id
    from grab_express.order_merchant_order_relations
    where concat(year,'-', month,'-', day) >= date_format(date_parse('2018-08-01', '%Y-%m-%d') -interval '1' day, '%Y-%m-%d')
    and concat(year,'-', month,'-', day) <= '2018-08-02'
  ) as invoice on invoice.order_id = orders_data.order_id

  /* to calculate GMV */
	left join (
		select booking_code, receipt_tolls_and_others, receipt_booking_fee
		from public.prejoin_bookings
		where taxi_type_id in (9447, 132)  /* need to change in future when SD 1.0 is launched in other countries */
		and partition_date >= date_format(date_parse('2018-08-01', '%Y-%m-%d') -interval '1' day, '%Y-%m-%d')
		and partition_date <= '2018-08-02'
	) as prejoin on prejoin.booking_code = orders_data.playbook_id

	/* to get exchange rate */
	left join (
    select country_id, exchange_one_usd from datamart.ref_exchange_rates
    where date_trunc ('month', date('2018-08-01')) = date_trunc ('month', end_date)
    and date_trunc ('year', date('2018-08-02')) = date_trunc ('year', end_date)
  ) as exchange on exchange.country_id = cities.country_id

	/* to determine if job was broadcasted to any candidates */
	left join (
	  select booking_code as code from public.candidates
	  where partition_date >= date_format(date_parse('2018-08-01', '%Y-%m-%d') -interval '1' day, '%Y-%m-%d')
		and partition_date <= '2018-08-02'
		and taxi_type_id in (9447, 132)  /* need to change in future when SD 1.0 is launched in other countries */
		and state_simple in ('Bid','Ignored','Declined')
	  group by 1
	) as cd on cd.code = orders_data.playbook_id

  group by 22,23,24,25,26,27,28,29,30,31,32

) as sameday_orders;
