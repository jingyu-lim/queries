-- Perfect Rides Last 30d
-- @vinhsoon.ta

create table product_analytics.vst_perfect_temp as
select
	booking_code
	, cast(created_at_local as timestamp) as created_at_local
	, passenger_id
	, driver_id
	, country
	, city
	, vertical
	, platform
	, T_app_load_time
	, T_clicks_to_book
	, T_time_to_allocate
	, T_pickup_ata_vs_eta
	, T_walking_distance
	, T_pax_dax_calls
	, T_destination_att_vs_ett
	, T_pax_rating
	, T_ce_calls
	, has_leanplum_match -- should be a partition
	, case when T_app_load_time is null or T_clicks_to_book is null or T_time_to_allocate is null or T_pickup_ata_vs_eta is null or T_walking_distance is null or T_pax_dax_calls is null or T_destination_att_vs_ett is null or T_pax_rating is null or T_ce_calls is null
		then 0 else 1 end as has_complete_data -- should be a partition
	, cast(created_at_local as date) as date_local -- should be a partition
	, hour_local -- should be a partition
from
	(
		select
			bkg.booking_code
			, bkg.created_at_local
			, bkg.hour_local
			, bkg.passenger_id
			, bkg.driver_id
			, bkg.country
			, bkg.city
			, bkg.vertical
			, bkg.platform
			, case when pdm.app_load_time is null then case when pdm.sessionid is null then null else 0 end else round(pdm.app_load_time/1000) end as T_app_load_time -- 0 if LP session is matched but no value available
			, case when pdm.clicks_to_book is null then null else pdm.clicks_to_book end as T_clicks_to_book
			, case when bkg.time_to_allocate is null then null else bkg.time_to_allocate end as T_time_to_allocate -- in seconds
			, case when eta.pickup_eta is null or eta.pickup_ata is null then null else abs(eta.pickup_eta - eta.pickup_ata) end as T_pickup_ata_vs_eta -- in seconds
			, case when eta.walking_distance is null then null else round(1000 * eta.walking_distance) end as T_walking_distance -- in meters
			, case when pdm.has_pax_call = 1 or ddm.has_dax_call = 1 then 1 else 0 end as T_pax_dax_calls
			, case when ett.destination_ett is null or att.destination_att is null then null else abs(ett.destination_ett - att.destination_att) end as T_destination_att_vs_ett -- in seconds
			, case when prt.rating is null or prt.rating = 5 then 5 else prt.rating end as T_pax_rating
			, case when cec.has_ce_ticket is not null or cec.has_ce_ticket = 1 then 1 else 0 end as T_ce_calls
			, case when pdm.sessionid is null then 0 else 1 end as has_leanplum_match
			, row_number() over (partition by pdm.sessionid order by bkg.booking_code asc, bkg.created_at_local asc) as booking_rank
		from
			(
				/* Backend Bookings */
				select
					pb.booking_code
					, pb.created_at_utc
					, pb.created_at_local
					, pb.hour_local
					, pb.passenger_id
					, pb.driver_id
					, co.name as country
					, ci.name as city
					, tt.taxi_type_simple as vertical
					, substr(pb.booking_code, 1, 3) as platform
					, date_diff('second', pb.created_at_local, pb.awarded_time) as time_to_allocate
					--, date_diff('second', pb.created_at_local, pb.estimated_pick_up_time) as pickup_eta
					--, date_diff('second', pb.estimated_pick_up_time, pb.estimated_drop_off_time) as pickup_ata
					--, date_diff('second', pb.created_at_local, pb.dropping_off_time) as destination_ett
					--, date_diff('second', pb.dropping_off_time, pb.completed_time) as destination_att
				from
					public.prejoin_bookings pb
					join public.countries co on co.id = pb.country_id
					join public.cities ci on ci.id = pb.city_id
					left join public.taxi_types_v tt on tt.id = pb.taxi_type_id and tt.start_at <= cast(pb.created_at_local as varchar) and tt.end_at > cast(pb.created_at_local as varchar)
				where
					concat(year, '-', month, '-', day) >= '2018-05-01'
					and concat(year, '-', month, '-', day) <= '2018-05-30'
					and pb.state in ('COMPLETED', 'AWARDED', 'ADVANCE_AWARDED', 'DROPPING_OFF', 'PICKING_UP')
					and tt.taxi_type_simple not in ('GrabFood', 'GrabExpress')
					and substr(pb.booking_code, 1, 3) in ('ADR', 'IOS')
				group by
					1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11
			) bkg

			left join (
				/* Leanplum PAX Bookings */
				select
					sessionid
					, passenger_id
					, array_position(filter(full_path, x -> x not like '%.DEFAULT' and x not like '%.NULL' and x not like 'SPLASH.%'), 'TRANSPORT_BOOKING.BOOK') + array_position(filter(full_path, x -> x not like '%.DEFAULT' and x not like '%.NULL' and x not like 'SPLASH.%'), 'HOMEPAGE.BOOK') as clicks_to_book
					, case when array_position(full_path, 'DRIVER_ON_THE_WAY.CONTACT_DRIVER') <> 0 then 1 else 0 end as has_pax_call
					, min_by(cast(element_at(event_parameters_map, 'SUBTEXT_3') as double), cardinality(full_path)) as app_load_time
					, min(case when eventname = 'BOOK' then eventstart end) as book_event_time
				from
					leanplum.pax_datamart_new
				where
					concat(year, '-', month, '-', day) >= '2018-05-01'
					and concat(year, '-', month, '-', day) <= '2018-05-30'
					and statename in ('SPLASH', 'HOMEPAGE', 'TRANSPORT_BOOKING', 'DRIVER_ON_THE_WAY')
					and eventname in ('APP_LOAD_SPEED', 'BOOK', 'CONTACT_DRIVER')
					and array_position(filter(full_path, x -> x not like '%.DEFAULT' and x not like '%.NULL' and x not like 'SPLASH.%'), 'TRANSPORT_BOOKING.BOOK') + array_position(filter(full_path, x -> x not like '%.DEFAULT' and x not like '%.NULL' and x not like 'SPLASH.%'), 'HOMEPAGE.BOOK') <> 0
				group by
					1, 2, 3, 4
			) pdm on pdm.passenger_id = bkg.passenger_id and date_add('second', -300, pdm.book_event_time) < cast(bkg.created_at_utc as timestamp) and date_add('second', 300, pdm.book_event_time) > cast(bkg.created_at_utc as timestamp)

			left join (
				/* Leanplum DAX calls */
				select
					element_at(event_parameters_map, 'BOOKING_CODE') as booking_code
					, 1 as has_dax_call
				from
					leanplum.dax_datamart_new
				where
					concat(year, '-', month, '-', day) >= '2018-05-01'
					and concat(year, '-', month, '-', day) <= '2018-05-30'
					and eventname = 'CALL'
				group by
					1, 2
			) ddm on ddm.booking_code = bkg.booking_code

			left join (
				/* ETA, ATA, Walking distance */
				select
					booking_code
					, first_chronos_eta_in_seconds as pickup_eta
					, start_ata_in_seconds + in_transit_ata_in_seconds as pickup_ata -- no end_ata_in_seconds
					, case
						when f_distance(cast(destination_lat as double), cast(destination_lon as double), inferred_pickup_dax_lat, inferred_pickup_dax_lon) < 1
						then f_distance(cast(destination_lat AS double), cast(destination_lon as double), inferred_pickup_dax_lat, inferred_pickup_dax_lon)
						else null end AS walking_distance
				from
					data_science.pickup_ada_ata_inferred
				where
					concat(year, '-', month, '-', day) >= '2018-05-01'
					and concat(year, '-', month, '-', day) <= '2018-05-30'
					and (start_ata_in_seconds + in_transit_ata_in_seconds + end_ata_in_seconds) is not null
				group by
					1, 2, 3, 4
			) eta on eta.booking_code = bkg.booking_code

			left join (
				/* ETT */
				select
					bookingcode as booking_code
					, duration as destination_ett
				from
					grab_datastore.pricing_fare p
					--join product_analytics.vst_perfect_rides_users fp on fp.id = p.passengerid
				where
					concat(year, '-', month, '-', day) >= '2018-05-01'
					and concat(year, '-', month, '-', day) <= '2018-05-30'
					and distance > 0
					and duration > 0
					and length(bookingcode) > 0
				group by
					1, 2
			) ett ON ett.booking_code = bkg.booking_code

			left join (
				/* ATT */
				select
					booking_code
					, att_inferred_seconds as destination_att
				from
					data_science.ride_adt_att_inferred
				where
					concat(year, '-', month, '-', day) >= '2018-05-01'
					and concat(year, '-', month, '-', day) <= '2018-05-30'
					and att_inferred_seconds is not null
					and adt_from_pings > 0
					and adt_from_pings/att_inferred_seconds < 0.056
				group by
					1, 2
			) att on att.booking_code = bkg.booking_code

			left join (
				/* PAX Rate Trip */
				select
					booking_code
					, rating
				from
					public.ratings r
					--join filter_passengers fp on fp.id = r.passenger_id
				where
					concat(year, '-', month, '-', day) >= '2018-05-01'
					and concat(year, '-', month, '-', day) <= '2018-05-30'
				group by
					1, 2
			) prt on prt.booking_code = bkg.booking_code


			left join (
				/* CE contacts */
				select
					booking_code
					, created_at
					, 1 as has_ce_ticket
				from
					zendesk.tickets
				where
					created_date >= '20180601'
					and created_date <= '20180625'
				group by
					1, 2, 3
			) cec on cec.booking_code = bkg.booking_code and cec.created_at >= cast(bkg.created_at_utc as timestamp) and date_add('day', -7, cec.created_at) < cast(bkg.created_at_utc as timestamp)
	) tmp
where
	(has_leanplum_match = 0 or (has_leanplum_match = 1 and booking_rank = 1))
