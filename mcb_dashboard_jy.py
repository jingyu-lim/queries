import sys
import time
from pyhive import presto
import pandas as pd
import datetime
from datetime import date

t0 = time.time()

print("Refreshing MCB dashboard")

conn = presto.connect(host='presto-product-analytics-02.data-engineering.myteksi.net', port='8889', catalog='hive', schema='public', poll_interval=1 , source='pyhive')

print("connection established")


# sql1 ="""drop table product_analytics.ad3_mcb_popln"""

sql1 ="""drop table product_analytics.logistics_mcb_popln"""

# CREATE table product_analytics.ad3_mcb_popln AS
sql2 = """
CREATE table product_analytics.logistics_mcb_popln AS
    select id as passenger_id
    ,  element_at(split(substr(user_agent, strpos(user_agent, '/')+1), ' '), 1) as application_version
    , substr(user_agent, strpos(user_agent, '(') +1 , 7) as opr_sys ,
    case when try(cast(substr( element_at(split(substr(user_agent, strpos(user_agent, '/')+1), ' '), 1), 1, 4) as double)) >= 4.51 then '1'
    else '0' end as test_segment
from public.passengers AS a

--INNER JOIN product_analytics.ad52_mcb_phase12 AS b ON b.passenger_id = CAST (a.id AS VARCHAR)

where lower(substr(user_agent, strpos(user_agent, '(') +1 , 7)) = 'android'

/* PH (id=2), VN (id=5) and ID (id=5) and TH (id=3) only */
and (country_id = 2 or country_id = 5 or country_id = 6 or country_id=3)
"""

# sql3 = """drop table product_analytics.ad3_mcb_conc_bookings"""

sql3 = """drop table product_analytics.logistics_mcb_conc_bookings"""

# CREATE table product_analytics.ad3_mcb_conc_bookings AS
sql4 = """
CREATE table product_analytics.logistics_mcb_conc_bookings AS
    SELECT bookings.code ,
    pickup_time as pickup_button_press_time
    , cast(dropoff_time as timestamp) as dropoff_button_press_time
    , bookings.created_at
    , application_version
    , bookings.state
    , os_detail.test_segment
    , opr_sys
    , bookings.passenger_id
    , bookings.date_local
    , ROW_NUMBER() OVER (PARTITION BY bookings.passenger_id ORDER BY bookings.created_at ) AS r_num
FROM public.bookings

/* filtering ge customers*/
INNER JOIN (SELECT id AS taxi_type_id FROM public.taxi_types_v WHERE taxi_type_simple= 'GrabExpress') AS ge_taxi_types
 ON bookings.taxi_type_id = ge_taxi_types.taxi_type_id
INNER JOIN product_analytics.logistics_mcb_popln AS os_detail ON CAST(os_detail.passenger_id AS VARCHAR) = CAST(bookings.passenger_id AS VARCHAR)
LEFT JOIN public.test_bookings AS c ON c.code = bookings.code AND c.code IS NULL

left join
(select booking_code,
picking_up_time as pickup_time ,
coalesce(completed_time, cancel_driver_time,
cancel_passenger_time,
cancel_ack_time ) as dropoff_time

from public.candidate_metadata

INNER JOIN (SELECT id AS taxi_type_id
FROM public.taxi_types_v
WHERE taxi_type_simple = 'GrabExpress') AS ge_taxi_types ON candidate_metadata.vehicle_type_id = cast(ge_taxi_types.taxi_type_id as varchar)
where year = '2018' and winner = 'true'
) as tt3 on tt3.booking_code = bookings.code


left join
(
select booking_code,
try(date_parse (REPLACE(REPLACE(CAST(json_extract (metadata,'$.dropOffTime') AS VARCHAR),'T',' '),'Z',''),'%Y-%m-%d %H:%i:%s')) as dropoff_time1,
try(date_parse (REPLACE(REPLACE(CAST(json_extract (metadata,'$.canceledTime') AS VARCHAR),'T',' '),'Z',''),'%Y-%m-%d %H:%i:%s')) as cancel_time1
from bs_db.booking
inner join product_analytics.logistics_mcb_popln as b on b.passenger_id = booking.passenger1_id
where year = '2018') as tt2 on tt2.booking_code = bookings.code

WHERE c.code IS NULL
AND   bookings.PARTITION_DATE >= '2018-05-10' and bookings.year = '2018'
/* and bookings.month IN ('05', '06') */
and bookings.date_local >= '2018-05-10'
and is_unique_booking = 't'
"""

# sql5 = """drop table product_analytics.ad3_mcb_bk_count"""

sql5 = """drop table product_analytics.logistics_mcb_bk_count"""

# CREATE TABLE product_analytics.ad3_mcb_bk_count AS
sql6 = """
CREATE TABLE product_analytics.logistics_mcb_bk_count AS
    SELECT t1.date_local
    ,t1.passenger_id
    ,t2.code AS concur_code
    ,t1.test_segment
    , CASE
    WHEN cast(t2.created_at as timestamp)>= cast(t1.created_at as timestamp)
    AND cast(t2.created_at as timestamp) <= cast(t1.dropoff_button_press_time as timestamp)
    THEN 1
    ELSE 0
    END AS concur_booking
    FROM product_analytics.logistics_mcb_conc_bookings AS t1
    LEFT JOIN product_analytics.logistics_mcb_conc_bookings AS t2 ON t1.passenger_id = t2.passenger_id
    AND t1.date_local = t2.date_local

    /* what does this line do? */
    AND t1.r_num +1 = t2.r_num
    WHERE t1.test_segment = '1'
    and t2.code is not null
GROUP BY
1,
2,
3,
4,
5
"""

# sql7 = """drop table product_analytics.ad3_conc_cohort"""

sql7 = """drop table product_analytics.logistics_conc_cohort"""

# CREATE TABLE product_analytics.ad3_conc_cohort AS
# eventually merge into sql10?
sql8 = """
CREATE TABLE product_analytics.logistics_conc_cohort AS
    SELECT
        passenger_id ,
        max(concur_booking) as concur_cohort
from product_analytics.logistics_mcb_bk_count
group by 1
"""

# sql9 = """drop table product_analytics.ad3_mcb_general_metrics"""

sql9 = """drop table product_analytics.logistics_mcb_general_metrics"""

# CREATE TABLE product_analytics.ad3_mcb_general_metrics AS
sql10 = """
CREATE TABLE product_analytics.logistics_mcb_general_metrics AS
SELECT DATE (DATE_PARSE (bookings.date_local,'%Y-%m-%d')) AS date_local,
CAST(bookings.hour_local AS INTEGER) AS hour_local,
bookings.city_id,
bookings.taxi_type_id,
bookings.passenger_id as pax_id,
cities.name as city,
cities.country_code as country,
coalesce(tt1.test_segment, '0') as test_segment,
coalesce(concur_cohort , 0) as cohort_perf,
COUNT(bookings.code) AS non_unique_bookings,

/* Unique Bookings */
SUM(CASE WHEN bookings.is_unique_booking = 't' THEN 1 ELSE 0 END) AS unique_bookings,

/* Unique concur Bookings */
SUM(CASE WHEN bookings.is_unique_booking = 't' and concur_booking = 1 THEN 1 ELSE 0 END) AS concur_UB,

/* Allocated Bookings */
SUM(CASE WHEN bookings.is_unique_booking = 't' AND
bookings.state IN ('ADVANCE_AWARDED','AWARDED','COMPLETED','DROPPING_OFF','PICKING_UP','CANCELLED_DRIVER','CANCELLED_OPERATOR','CANCELLED_PASSENGER')
THEN 1 ELSE 0 END) AS allocated_bookings,

/* concur Allocated Bookings */
SUM(CASE WHEN concur_booking = 1 and bookings.is_unique_booking = 't' AND
bookings.state IN ('ADVANCE_AWARDED','AWARDED','COMPLETED','DROPPING_OFF','PICKING_UP','CANCELLED_DRIVER','CANCELLED_OPERATOR','CANCELLED_PASSENGER')
THEN 1 ELSE 0 END) AS concur_allocated_bk,

/* Unallocated Bookings */
SUM(CASE WHEN bookings.is_unique_booking = 't' AND
bookings.state NOT IN ('ADVANCE_AWARDED','AWARDED','COMPLETED','DROPPING_OFF','PICKING_UP','CANCELLED_DRIVER','CANCELLED_OPERATOR','CANCELLED_PASSENGER')
THEN 1 ELSE 0 END) AS unallocated_bookings,

/* concur Unallocated Bookings */
SUM(CASE WHEN concur_booking = 1 and bookings.is_unique_booking = 't'
AND bookings.state NOT IN ('ADVANCE_AWARDED','AWARDED','COMPLETED','DROPPING_OFF','PICKING_UP','CANCELLED_DRIVER','CANCELLED_OPERATOR','CANCELLED_PASSENGER')
THEN 1 ELSE 0 END) AS concur_unallocated_bk,

/* Bookings Cancelled By Driver */
SUM(CASE WHEN bookings.state = 'CANCELLED_DRIVER' THEN 1 ELSE 0 END) AS bookings_cancelled_by_driver,

/* Bookings Cancelled By Driver */
SUM(CASE WHEN concur_booking = 1 and bookings.state = 'CANCELLED_DRIVER' THEN 1 ELSE 0 END) AS concur_bk_cancelled_by_dax,

/* Bookings Cancelled By Driver */
SUM(CASE WHEN bookings.state = 'CANCELLED_OPERATOR' THEN 1 ELSE 0 END) AS bookings_cancelled_by_operator,

/* Bookings Cancelled By Operator */
SUM(CASE WHEN concur_booking = 1 and bookings.state = 'CANCELLED_OPERATOR' THEN 1 ELSE 0 END) AS concur_bk_cancelled_by_opr,

/* Bookings Cancelled By Passenger */
SUM(CASE WHEN bookings.state = 'CANCELLED_PASSENGER' THEN 1 ELSE 0 END) AS bookings_cancelled_by_passenger,

/* Bookings Cancelled By Passenger */
SUM(CASE WHEN concur_booking = 1 and bookings.state = 'CANCELLED_PASSENGER' THEN 1 ELSE 0 END) AS concur_bk_cancelled_by_pax,

/* Rides */
SUM(CASE WHEN bookings.state IN ('ADVANCE_AWARDED','AWARDED','COMPLETED','DROPPING_OFF','PICKING_UP') THEN 1 ELSE 0 END) AS rides,

/* Concur Rides */
SUM(CASE WHEN bookings.is_unique_booking = 't' and concur_booking = 1  and
bookings.state IN ('ADVANCE_AWARDED','AWARDED','COMPLETED','DROPPING_OFF','PICKING_UP') THEN 1 ELSE 0 END) AS concur_rides,

/*GMV*/
SUM(case WHEN bookings.state IN ('ADVANCE_AWARDED','AWARDED','COMPLETED','DROPPING_OFF','PICKING_UP')
then bookings.fare +COALESCE(receipts.tolls_and_surcharges,0) else 0 end ) AS gmv,

/*concur GMV*/
SUM(case WHEN concur_booking = 1 and bookings.state IN ('ADVANCE_AWARDED','AWARDED','COMPLETED','DROPPING_OFF','PICKING_UP')
then bookings.fare +COALESCE(receipts.tolls_and_surcharges,0) else 0 end ) AS concur_gmv,

/* First Allocated Rides */
SUM(CASE WHEN bookings.state IN ('ADVANCE_AWARDED','AWARDED','COMPLETED','DROPPING_OFF','PICKING_UP') AND unique_bookings.first_allocated = 't' THEN 1 ELSE 0 END) AS first_allocated_rides,                      /* First Allocated Bookings */ SUM(CASE WHEN unique_bookings.first_allocated = 't' THEN 1 ELSE 0 END) AS first_allocated_bookings ,

/* concur First Allocated Rides */
SUM(CASE WHEN concur_booking = 1  and
bookings.state IN ('ADVANCE_AWARDED','AWARDED','COMPLETED','DROPPING_OFF','PICKING_UP') AND unique_bookings.first_allocated = 't' THEN 1 ELSE 0 END)
AS concur_first_allocated_rides,

/* concur  First Allocated Bookings */
SUM(CASE WHEN concur_booking = 1  and  unique_bookings.first_allocated = 't' THEN 1 ELSE 0 END)
AS concur_first_allocated_bk
FROM public.bookings
left join public.cities on cities.id = bookings.city_id
INNER JOIN (SELECT id AS taxi_type_id
FROM public.taxi_types_v
WHERE taxi_type_simple = 'GrabExpress') AS ge_taxi_types ON bookings.taxi_type_id = ge_taxi_types.taxi_type_id
left join grab_express.merchants on cast(merchants.merchant_grab_id as varchar) = cast(substr(bookings.code, 9, 8) as varchar)

inner join product_analytics.logistics_mcb_popln as os_detail on cast(os_detail.passenger_id as varchar) = cast(bookings.passenger_id as varchar)
left join product_analytics.logistics_mcb_bk_count  as tt1 on tt1.concur_code = bookings.code
left join product_analytics.logistics_conc_cohort as tt2 on tt2.passenger_id = bookings.passenger_id

LEFT JOIN test_bookings AS c
ON c.code = bookings.code
AND c.code IS NULL
LEFT JOIN (SELECT booking_code,
app_booking_fee,
tolls_and_surcharges
FROM public.receipts
WHERE CONCAT(receipts.year, '-', receipts.month, '-', receipts.day) >= DATE_FORMAT(DATE_PARSE('2018-05-11', '%Y-%m-%d') -INTERVAL '1' DAY, '%Y-%m-%d')
) receipts ON bookings.code = receipts.booking_code
LEFT JOIN (SELECT last_id,
first_allocated
FROM public.unique_bookings
WHERE CONCAT(unique_bookings.year, '-', unique_bookings.month, '-', unique_bookings.day) >= DATE_FORMAT(DATE_PARSE('2018-05-11', '%Y-%m-%d') -INTERVAL '1' DAY, '%Y-%m-%d')
GROUP BY last_id,
first_allocated) unique_bookings ON bookings.id = unique_bookings.last_id
WHERE c.code IS NULL
AND   bookings.PARTITION_DATE >= '2018-05-01' and bookings.year = '2018' and bookings.date_local >= '2018-05-10'
GROUP BY
1,
2,
3,
4,
5,
6,
7,
8,
9
"""

#allsql = [sql1, sql2, sql3, sql4, sql5, sql6, sql7, sql8, sql9, sql10]
#messages = ["sql1", "sql2", "sql3", "sql4", "sql5", "sql6", "sql7", "sql8", "sql9", "sql10"]

allsql = [sql3, sql4, sql5, sql6, sql7, sql8, sql9, sql10]
messages = ["sql3", "sql4", "sql5", "sql6", "sql7", "sql8", "sql9", "sql10"]

#allsql = [sql8, sql10]
#messages = ["sql8", "sql10"]

last = len(allsql) - 1
i = 0
print("starting execution of script")
while (i <= last):
   try:
       print("Executing " + messages[i] + "\n")
       pd.read_sql_query(allsql[i], conn)

   except Exception as e:
       print("Error: \n" + str(e) + "\n")
   i = i + 1

print("Finished execution of script")
print ("script completed running in %5.2f s" % (time.time()-t0))
